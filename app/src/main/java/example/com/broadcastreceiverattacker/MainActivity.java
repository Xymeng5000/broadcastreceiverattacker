package example.com.broadcastreceiverattacker;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button) findViewById(R.id.button1);
    }

    public void onClick(View v) {
                //if(v.getId()==R.id.button1){
        Intent intent = new Intent();
        //intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.putExtra("number", 1);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setComponent(new ComponentName("example.com.broadcastreceiver","example.com.broadcastreceiver.MyBroadCastReceiver"));
        intent.setAction("com.example.MyBroadcast");
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
       // if(isIntentSafe) {
            //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
            sendBroadcast(intent);
      //  }
    }
}
